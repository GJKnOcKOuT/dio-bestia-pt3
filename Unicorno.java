package com.gioco.sos.Gioco;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Unicorno {
	private int x;
	private int y;
	private int larghezza;
	private int altezza;
	private boolean attivo=false;
	BufferedImage img_unicorno;
	public Unicorno(BufferedImage image,int larghezza,int altezza,int x,int y ) {
	this.x=x;
	this.y=y;
	this.larghezza=larghezza;
	this.altezza=altezza;
	this.img_unicorno=image;
	attivo=true;
	}
public void run(){
	attivo=true;
	while(attivo) {
	aggiorna();
	try {
		Thread.sleep(100);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
}
private void aggiorna() {
	//Si muove
	x++;
}
void disegna(Graphics g) {
	//si disegna
	g.drawImage(img_unicorno,x,y,larghezza,altezza,null);
	
}
}
